import React from 'react';
import Letter from "./Letter";

interface BoardProps {
    board: string[][]
}

const Board: React.FC<BoardProps> = (props) => {
    const board = props.board;

    return (
        <div className="wordle-grid" id="wordle-grid">
            {/* Row 1 */}
            <Letter attempt={0} position={0} board={board}/>
            <Letter attempt={0} position={1} board={board}/>
            <Letter attempt={0} position={2} board={board}/>
            <Letter attempt={0} position={3} board={board}/>
            <Letter attempt={0} position={4} board={board}/>
            {/* Row 2 */}
            <Letter attempt={1} position={0} board={board}/>
            <Letter attempt={1} position={1} board={board}/>
            <Letter attempt={1} position={2} board={board}/>
            <Letter attempt={1} position={3} board={board}/>
            <Letter attempt={1} position={4} board={board}/>
            {/* Row 3 */}
            <Letter attempt={2} position={0} board={board}/>
            <Letter attempt={2} position={1} board={board}/>
            <Letter attempt={2} position={2} board={board}/>
            <Letter attempt={2} position={3} board={board}/>
            <Letter attempt={2} position={4} board={board}/>
            {/* Row 4 */}
            <Letter attempt={3} position={0} board={board}/>
            <Letter attempt={3} position={1} board={board}/>
            <Letter attempt={3} position={2} board={board}/>
            <Letter attempt={3} position={3} board={board}/>
            <Letter attempt={3} position={4} board={board}/>
            {/* Row 5 */}
            <Letter attempt={4} position={0} board={board}/>
            <Letter attempt={4} position={1} board={board}/>
            <Letter attempt={4} position={2} board={board}/>
            <Letter attempt={4} position={3} board={board}/>
            <Letter attempt={4} position={4} board={board}/>
            {/* Row 6 */}
            <Letter attempt={5} position={0} board={board}/>
            <Letter attempt={5} position={1} board={board}/>
            <Letter attempt={5} position={2} board={board}/>
            <Letter attempt={5} position={3} board={board}/>
            <Letter attempt={5} position={4} board={board}/>

            {/*<div className="tile" wordle-state="active" ></div>*/}
            {/*<div className="tile" wordle-state="wrong"></div>*/}
            {/*<div className="tile" wordle-state="wrong-location"></div>*/}
            {/*<div className="tile" wordle-state="correct"></div>*/}
            {/*<Letter letter="D"/>*/}
        </div>
    );
};

export default Board;
