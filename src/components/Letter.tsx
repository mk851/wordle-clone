import React from 'react';

interface LetterProps {
    attempt: number
    position: number
    board: string[][]
}

const Letter: React.FC<LetterProps> = (props) => {
    const myLetter = props.board[props.attempt][props.position];
    return (
        <div className="tile" wordle-state="active">
            {myLetter}
        </div>
    );
};

export default Letter;
